﻿
function start_processing() {
    console.log("Fetching data...");
    //$.ajax({
    //    type: "GET",
    //    url: "openpowerliftingdata.csv",
    //    dataType: "text",
    //    success: function (data) { csvJSON(data); }
    //});

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            csvJSON(this.responseText);
        }
    };
    xhttp.open("GET", "openpowerliftingdata.csv", true);
    xhttp.send();
}

function csvJSON(csv) {
    console.log("Processing data...");
    var lines = csv.split("\n");
    var result = [];
    var headers = lines[0].split(",");

    for (var i = 1; i < lines.length; i++) {

        var obj = {};
        var currentline = lines[i].split(",");

        for (var j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j];
        }
        result.push(obj);
    }
    //return result; //JavaScript object
    postMessage(result);
    // return result; //JSON
}

start_processing();