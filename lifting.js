﻿
var lifters = [];

// JavaScript source cod
$(document).ready(function () {
    var input = document.getElementById("numDeadliftWeight");
    input.addEventListener("keyup", function (event) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Trigger the button element with a click
            document.getElementById("btnDefault").click();
        }
    });

    var input = document.getElementById("numBenchWeight");
    input.addEventListener("keyup", function (event) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Trigger the button element with a click
            document.getElementById("btnDefault").click();
        }
    });

    var input = document.getElementById("numSquatWeight");
    input.addEventListener("keyup", function (event) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Trigger the button element with a click
            document.getElementById("btnDefault").click();
        }
    });

    var input = document.getElementById("numBodyWeight");
    input.addEventListener("keyup", function (event) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Trigger the button element with a click
            document.getElementById("btnDefault").click();
        }
    })
    
    startWorker();



    //$.ajax({
    //    type: "GET",
    //    url: "openpowerliftingdata.csv",
    //    dataType: "text",
    //    success: function(data) {csvJSON(data);}
    //});
    //config = {
    //    delimiter: "",	// auto-detect
    //    newline: "",	// auto-detect
    //    quoteChar: '"',
    //    escapeChar: '"',
    //    header: false,
    //    trimHeader: false,
    //    dynamicTyping: false,
    //    preview: 0,
    //    encoding: "",
    //    worker: false,
    //    comments: false,
    //    step: undefined,
    //    complete: undefined,
    //    error: undefined,
    //    download: false,
    //    skipEmptyLines: false,
    //    chunk: undefined,
    //    fastMode: undefined,
    //    beforeFirstChunk: undefined,
    //    withCredentials: undefined
    //}

    //var file = new File("http://localhost:50120/OpenPowerliftingData.csv", "OpenPowerliftingData.csv");

    //var data = Papa.parse(file, config);

    //console.log(data.data);

});

function startWorker() {
    var w;
    if (typeof (Worker) !== "undefined") {
        if (typeof (w) == "undefined") {
            w = new Worker("DataWorker.js");
        }
        w.onmessage = function (event) {
            lifters = event.data;
        };
    } else {
        document.getElementById("result").innerHTML = "Sorry! No Web Worker support... Loading data might take a bit longer than usual.";
    }
}

function processData(allText) {
    var record_num = 5;  // or however many elements there are in each row
    var allTextLines = allText.split(/\r\n|\n/);
    var entries = allTextLines[0].split(',');
    var lines = [];

    var headings = entries.splice(0,record_num);
    while (entries.length>0) {
        var tarr = [];
        for (var j=0; j<record_num; j++) {
            tarr.push(headings[j]+":"+entries.shift());
        }
        lines.push(tarr);
    }
    ////alert(lines);
}

function csvJSON(csv){
    var lines=csv.split("\n");
    var result = [];
    var headers=lines[0].split(",");

    for(var i=1;i<lines.length;i++){

        var obj = {};
        var currentline=lines[i].split(",");

        for(var j=0;j<headers.length;j++){
            obj[headers[j]] = currentline[j];
        }
        result.push(obj);
    }
    //return result; //JavaScript object
    lifters = result;
   // return result; //JSON
}

var Total_weightRatio;
var Squat_weightRatio;
var Benchpress_weightRatio;
var Deadlift_weightRatio;


var Total_userBin;
var Squat_userBin;
var Benchpress_userBin;
var Deadlift_userBin;

var conversion = 0.453592;

function filter_lifters() {
    //form input values
    var inputAge = Number($("#numAge").val());
    var inputSex = $("input[type=radio][name=sex]:checked").val();
    var inputBodyWeight = Number($("#numBodyWeight").val());
    var inputSquat = Number($("#numSquatWeight").val());
    var inputBench =Number($("#numBenchWeight").val());
    var inputDeadlift = Number($("#numDeadliftWeight").val());
    var inputEquipment = $("select#selectEquipment").val();
    var inputUnits = Number($("select#selectUnits").val());

    //calculated values from user
    if (inputUnits == 2) {
        inputBodyWeight = inputBodyWeight * conversion;
        inputSquat = inputSquat * conversion;
        inputBench = inputBench * conversion;
        inputDeadlift = inputDeadlift * conversion;
    }

    var inputTotal = inputSquat + inputBench + inputDeadlift;

    Total_weightRatio = inputTotal / inputBodyWeight;
    Squat_weightRatio = inputSquat / inputBodyWeight;
    Benchpress_weightRatio = inputBench / inputBodyWeight;
    Deadlift_weightRatio = inputDeadlift / inputBodyWeight;

    //var gender = 0;
    //var raw = 0;
    //var geared = 0;
    //var young = 0;
    //var old = 0;

    //var Sex = "";
    //var Equipment = "";
    //var Age = 0;
    //var Division = 0;
    //var BodyweightKg = 0;
    //var WeightClassKg = 0;
    //var Squat4Kg = 0;
    //var BestSquatKg = 0;
    //var Bench4Kg = 0;
    //var BestBenchKg = 0;
    //var Deadlift4Kg = 0;
    //var BestDeadliftKg = 0;
    //var TotalKg = 0;
    //var Place = 0;
    //var Wilks = 0;

    //var squatbw = BestSquatKg / BodyweightKg;
    //var benchbw = BestBenchKg / BodyweightKg;
    //var deadbw = BestDeadliftKg / BodyweightKg;
    //var totalbw = TotalKg / BodyweightKg;
    //var bodyweightlb = BodyweightKg * 2.2;

    var gear = [];

    if (inputEquipment == "Raw")
        gear = ["Raw"];
    if (inputEquipment == "Wraps") {
        gear =  ["Raw","Wraps","Straps"];
    }
    if (inputEquipment == "Geared") {
        gear = ["Single-ply","Multi-ply"];
    }
    //if (Age < 23)
    //    young = 1;

    //if (Age > 40)
    //    old = 1;

    //Gender, Equipment, Weight, Drug tested**
    //evar bwc=group(weightclasskg)

    //var totalmean = 0;
    //var squatmean = 0;
    //var benchmean = 0;
    //var deadmean = 0;
    //var totalsd = 0;
    //var squatsd = 0;
    //var benchsd = 0;
    //var deadsd = 0;

    //bysort bwc raw fullmeet: evar totalmean=mean(totalbw)
    //bysort bwc raw: evar squatmean=mean(squatbw)
    //bysort bwc raw: evar benchmean=mean(benchbw)
    //bysort bwc raw: evar deadmean=mean(deadbw)
    //bysort bwc raw fullmeet: evar totalsd=sd(totalbw)
    //bysort bwc raw: evar squatsd=sd(squatbw)
    //bysort bwc raw: evar benchsd=sd(benchbw)
    //bysort bwc raw: evar deadsd=sd(deadbw)

    //alert(lifters.length);
   //alert(gear);
    var filtered_lifters = lifters.filter(function (lifter, index, lifters) {
        return gear.includes(lifter.Equipment) && lifter.Sex == inputSex;
    });

    var classed_lifters = filtered_lifters.filter(function (lifter) {
        var isIncluded = WeightClass(lifter.BodyweightKg, lifter.Sex) == WeightClass(inputBodyWeight, inputSex)
        return isIncluded;
    });

    var Full_Meet_lifters = classed_lifters.filter(function (lifter) {
        var isIncluded = lifter.BestSquatKg > 0 && lifter.BestBenchKg > 0 && lifter.BestDeadliftKg > 0 && lifter.BestSquatKg != null && lifter.BestBenchKg != null && lifter.BestDeadliftKg != null;
        return isIncluded;
    });

    var Squat_lifters = classed_lifters.filter(function (lifter) {
        var isIncluded = lifter.BestSquatKg > 0  && lifter.BestSquatKg != null;
        return isIncluded;
    });

    var Dealift_lifters = classed_lifters.filter(function (lifter) {
        var isIncluded = lifter.BestDeadliftKg > 0 && lifter.BestDeadliftKg != null;
        return isIncluded;
    });

    var Bench_lifters = classed_lifters.filter(function (lifter) {
        var isIncluded = lifter.BestBenchKg > 0  && lifter.BestBenchKg != null;
        return isIncluded;
    });


   //alert("lifters by Sex and Equipment: " + filtered_lifters.length);
   //alert("lifters by Weight class: " + classed_lifters.length);
   //alert("lifters by Full Meet: " + Full_Meet_lifters.length);

    //var calc_weight;
    //var calc_gender;

    //var totalcv = totalsd / totalmean;
    //var squatcv = squatsd / squatmean;
    //var benchcv = benchsd / benchmean;
    //var deadcv = deadsd / deadmean;

    //var totalscore = (totalbw - totalmean) / totalsd;
    //var squatscore = (squatbw - squatmean) / squatsd;
    //var benchscore = (benchbw - benchmean) / benchsd;
    //var deadscore = (deadbw - deadmean) / deadsd;

    //var totalp=normal(totalscore)
    //var squatp=normal(squatscore)
    //var benchp=normal(benchscore)
    //var deadp=normal(deadscore)

    //var totalwilks = 350 + totalscore * 75;
    //var squatwilks = 350 + squatscore * 25;
    //var benchwilks = 350 + benchscore * 25;
    //var deadwilks = 350 + deadscore * 25;

    //var total_above_avg=(totalp-.5)*100
    //var squat_above_avg=(squatp-.5)*100
    //var bench_above_avg=(benchp-.5)*100
    //var dead_above_avg=(deadp-.5)*100

    //var top_total=1-totalp
    //var top_squat=1-squatp
    //var top_bench=1-benchp
    //var top_dead=1-deadp

    var Full_meet_lifts = Full_Meet_lifters.map(a => a.TotalKg / a.BodyweightKg);
    var Squat_lifts = Squat_lifters.map(a => a.BestSquatKg / a.BodyweightKg);
    var Benchpress_lifts = Bench_lifters.map(a => a.BestBenchKg / a.BodyweightKg);
    var Deadlift_lifts = Dealift_lifters.map(a => a.BestDeadliftKg / a.BodyweightKg);

    var Existing_Full_meet_lifts = Non_null_lifts(Full_meet_lifts);
    var Existing_Squat_lifts = Non_null_lifts(Squat_lifts);
    var Existing_Benchpress_lifts = Non_null_lifts(Benchpress_lifts);
    var Existing_Deadlift_lifts = Non_null_lifts(Deadlift_lifts);

   //alert("number of weights: " + weights.length);
    //alert("non-null entries: " + existing_weights.length);
    var User_Lift_Information = [];

    var Total_info = Calculate_Lift_Statistics(Existing_Full_meet_lifts, Total_weightRatio);
    var Squat_info = Calculate_Lift_Statistics(Existing_Squat_lifts, Squat_weightRatio);
    var Benchpress_info = Calculate_Lift_Statistics(Existing_Benchpress_lifts, Benchpress_weightRatio);
    var Deadlift_info = Calculate_Lift_Statistics(Existing_Deadlift_lifts, Deadlift_weightRatio);
    //alert("standard deviation: " + std);

    generateNormalHistogram(51, Existing_Full_meet_lifts);

    $("#Total_userScore").text(Total_info.User_Score);
    $("#Total_stdDev").text(Total_info.Total_StdDev);
    $("#Total_avgRatio").text(Total_info.Total_Avg);

    
    $("#Squat_userScore").text(Squat_info.User_Score);
    $("#Squat_stdDev").text(Squat_info.Total_StdDev);
    $("#Squat_avgRatio").text(Squat_info.Total_Avg);

    $("#Benchpress_userScore").text(Benchpress_info.User_Score);
    $("#Benchpress_stdDev").text(Benchpress_info.Total_StdDev);
    $("#Benchpress_avgRatio").text(Benchpress_info.Total_Avg);

    $("#Deadlift_userScore").text(Deadlift_info.User_Score);
    $("#Deadlift_stdDev").text(Deadlift_info.Total_StdDev);
    $("#Deadlift_avgRatio").text(Deadlift_info.Total_Avg);

}

function round(number, precision) {
    var shift = function (number, precision) {
        var numArray = ("" + number).split("e");
        return +(numArray[0] + "e" + (numArray[1] ? (+numArray[1] + precision) : precision));
    };
    return shift(Math.round(shift(number, +precision)), -precision);
}

function Calculate_User_Score(User_Deviation) {
    return round(100 + (User_Deviation * 10), 2)
}

function Calculate_Lift_Statistics(lifts, user_input) {
    var All_lifters = standardDeviation(lifts);
    var User_Deviation = round((user_input - All_lifters.Avg) / All_lifters.StdDev, 2);
    var User_Score = Calculate_User_Score(User_Deviation)

    generateNormalHistogram(51, lifts);

    return ({ Total_Avg: round(All_lifters.Avg,2), Total_StdDev: round(All_lifters.StdDev,2), User_Dev: User_Deviation, User_Score: User_Score});
}

function Non_null_lifts(list) {
    return list.filter(function (elem) {
        return elem != null && elem > 0;
    });
}

function WeightClass(bodyWeight, sex) {
    var weights = [];
    var value;

    if (sex = "M") {
        if (bodyWeight > 120) {
            return '120+'
        }
        weights = [0, 53, 59, 66, 74, 83, 93, 105, 120];
    } else {
        if (bodyWeight > 84) {
            return '84+'
        }
        weights = [0, 43, 47, 52, 57, 63, 72, 84];
    }

    if (weights[0] == 0) {
        for (i = 0; i <= weights.length; i++) {
            if (bodyWeight <= weights[i] && bodyWeight > weights[i - 1]) {
                value = weights[i];
                break;
            }
        }
    } else {
        return "no class";
    }

    return value;
}


function standardDeviation(values) {
    var avg = average(values);
   //alert("Average: " + avg);
    var squareDiffs = values.map(function (value) {
        var diff = Number(value) - avg;
        var sqrDiff = diff * diff;
        return sqrDiff;
    });

    var avgSquareDiff = average(squareDiffs);

    var stdDev = Math.sqrt(avgSquareDiff);
    return ({StdDev: stdDev, Avg:avg});
}

function average(data) {
    var sum = data.reduce(function (sum, value) {
        return sum + Number(value);
    }, 0);

    var avg = sum / data.length;
    return avg;
}

var data = [];

function generateNormalHistogram(binSize, lifter_data) {
    data = [];

    //setting up empty data array
    getData(binSize, lifter_data); // popuate data 

    var Weight_Categories = data.map(x => x.weight);
    var string_data = JSON.stringify(data);
    // line chart based on http://bl.ocks.org/mbostock/3883245
   //alert(string_data);
    var chart = c3.generate({
        bindto: '#barChart',
        data: {
            json:data,
            keys: {
                //x: 'name', // it's possible to specify 'x' when category axis
                value: ['n'],
            },
            type: 'bar',
            color: function (inColor, data) {
                if (data.index == userBin) {
                    return d3.rgb(inColor).darker(1);
                } else {
                    return inColor;
                } 
            }
        },
        bar: {
            width: {
                ratio: .85
            }
        },
        axis: {
            x: {
                type: 'category',
                label: 'Total Weight Lifted / Body Weight',
                categories:Weight_Categories
            },
            y: {
                label: 'Observations (n)'
            }
        },
        legend: {
            hide:true
        }
    });
}

function getData(binSize, lifter_data) {
    var i;

    var binNum = binSize, valNum = lifter_data.length;
    var rValues = lifter_data.map(x => Number(x));
    var bins = [], sums = [];

   //alert("rValues: " + rValues);

    for (i = 0; i < binNum; i++) {
        bins.push(0);
        sums.push(0);
    }

   //alert("bins: " + bins);

    var rRange = d3.extent(rValues, function (d) {
        return d;
    });

   //alert("rRange: " + rRange);

    for (i = 0; i < valNum; i++) {
        var bin = Math.floor((rValues[i] - rRange[0]) / ((rRange[1] - rRange[0]) / (binNum - 1)));
        sums[bin] += Number(rValues[i]);
        bins[bin] += 1;
    }

    userBin = Math.floor((Total_weightRatio - rRange[0]) / ((rRange[1] - rRange[0]) / (binNum - 1)));
   //alert("n: " + bins);

    var binInterval = ((rRange[1] - rRange[0]) / (binNum - 1));
    for (i = 0; i < binNum; i++) {
        data.push({ q: Math.round((binInterval * i + rRange[0]) * 10) / 10, n: bins[i], weight: Math.round(sums[i] / bins[i]) });
    }
}
